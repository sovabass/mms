@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="col-sm-8 p-2 text-center">
            @if (\Session::has('success'))
            <div class="alert alert-success ">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
            @endif
        </div>
        <form class="col-sm-8" method="POST" action="{{route('employees.update',$employee->id)}}"
            enctype="multipart/form-data">
            {{csrf_field()}}
            @method('PUT')
            <div class="form-group">
                <label for="name">Practice</label>
                <select name="practice_id" class="form-control">
                    @foreach($practices as $practice)
                    @if($practice->id==$employee->practice->id)
                    <option value="{{$practice->id}}" selected="true">{{$practice->name}}</option>
                    @else
                    <option value="{{$practice->id}}">{{$practice->name}}</option>
                    @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="name">First Name</label>
                <input type="text" class="form-control" placeholder="Enter first name"
                    value="{{ $employee->first_name }}" required name="first_name">
            </div>
            <div class="form-group">
                <label for="email">Last name</label>
                <input type="email" class="form-control" placeholder="Enter last name" value="{{ $employee->email }}"
                    required name="last_name">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" placeholder="Enter email" value="{{ $employee->email }}"
                    name="email">
            </div>
            <div class="form-group">
                <label for="email">Phone</label>
                <input type="text" class="form-control" placeholder="Enter phone" value="{{ $employee->phone }}"
                    name="phone">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection