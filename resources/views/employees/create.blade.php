@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


        <div class="col-sm-8 p-2 text-center">
            @if (\Session::has('success'))
            <div class="alert alert-success ">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
            @endif
        </div>

        <form class="col-sm-8" method="POST" action="{{route('employees.store')}}" enctype="multipart/form-data">
             
            <div class="form-group">
                {{csrf_field()}}
                <label for="name">Practice</label>
                <select name="practice_id" class="form-control">
                    @foreach($practices as $practice)                 
                    <option value="{{$practice->id}}">{{$practice->name}}</option>      
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="name">First Name</label>
                <input type="text" class="form-control" placeholder="Enter first name"
                   required name="first_name">
            </div>
            <div class="form-group">
                <label for="text">Last name</label>
                <input type="text" class="form-control" placeholder="Enter last name"
                    required name="last_name">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" placeholder="Enter email" 
                    name="email">
            </div>
            <div class="form-group">
                <label for="email">Phone</label>
                <input type="text" class="form-control" placeholder="Enter phone" 
                    name="phone">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection