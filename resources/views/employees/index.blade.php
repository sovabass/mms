@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-10 text-center">
            @if (\Session::has('success'))
            <div class="alert alert-success ">
                <p>{{ \Session::get('success') }}</p>
            </div>
            @endif

        </div>
        <div class="col-sm-2 text-right ">
            <a class="btn-danger btn my-2" href="{{route('employees.create')}}"> New Record</a>
        </div>
    </div>
    <div class="row justify-content-center">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Practices</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Details</th>
                    <th scope="col">Edit</th>
                  
                    
                </tr>
            </thead>
            <tbody>
                @if(sizeof($employees)>0)
                @foreach($employees as $employee)
                <tr class="">  
                    <th scope="row">{{$employee->id}}</th>
                    <td>{{$employee->practice->name}}</td>
                    <td>{{$employee->first_name}}</td>
                    <td>{{$employee->last_name}}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->phone}}</td>
                    <td><a href="{{route('employees.show',['id'=>$employee->id])}}" class="btn btn-sm btn-info">View</a></td>
                    <td><a href="{{route('employees.edit',['id'=>$employee->id])}}" class="btn btn-sm btn-primary">Edit</a></td>
                    
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $employees->links() }}
        @else
        <div class="col-md-12 alert alert-danger text-center">No records</div>
        @endif
    </div>
</div>
@endsection