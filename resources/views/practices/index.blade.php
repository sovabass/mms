@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-10 text-center">
            @if (\Session::has('success'))
            <div class="alert alert-success ">
                <p>{{ \Session::get('success') }}</p>
            </div>
            @endif

        </div>
        <div class="col-sm-2 text-right ">
            <a class="btn-danger btn my-2" href="{{route('practices.create')}}"> New Record</a>
        </div>
    </div>
    <div class="row justify-content-center">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Logo</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    
                    <th scope="col">Website</th>
                    <th scope="col">Tags</th>
                    <th scope="col">Edit</th>
                </tr>
            </thead>
            <tbody>
                @if(sizeof($practices)>0)
                @foreach($practices as $practice)
                <tr class="">
                    <th scope="row">{{$practice->id}}</th>
                      <td><img class="logo" src="{{$practice->avatar}}" /></td>
                    <td><a class="text-capitalize"
                            href="{{route('practices.show',$practice->id)}}">{{$practice->name}}</a></td>
                    <td>{{$practice->email}}</td>
                  
                    <td><a href="{{$practice->website}}" target="_blank">Link</a></td>
                    <td><span class="text-success"> {{$practice->fields}}</span></td>
                    <td><a href="{{route('practices.edit',['id'=>$practice->id])}}"
                            class="btn btn-sm btn-primary">Edit</a></td>

                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $practices->links() }}
        @else
        <div class="col-md-12 alert alert-danger text-center">No records</div>
        @endif
    </div>
</div>
@endsection