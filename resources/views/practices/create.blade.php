@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


        <div class="col-sm-8 p-2 text-center">
            @if (\Session::has('success'))
            <div class="alert alert-success ">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
            @endif
        </div>

        <form class="col-sm-8" method="POST" action="{{route('practices.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control"  placeholder="Enter name" value=""
                    required name="name">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control"  placeholder="Enter email" value=""
                    required name="email">
            </div>
            <div class="form-group">
                <label for="name">Website</label>
                <input type="text" class="form-control"  placeholder="Enter `website link" value=""
                    name="website">
            </div>
            <div class="form-group">
                <label for="logo">Logo</label>
                <input type="file" name="logo" class="form-control">
            </div>
            <div class="form-group">
                @foreach($tags as $tag)
                <div class="form-check">
                    <label>
                        <input type="checkbox" name="tags[]"  value="{{$tag->id}}"> <span class="label-text text-capitalize">{{$tag->tag}}</span>
                    </label>
                </div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection