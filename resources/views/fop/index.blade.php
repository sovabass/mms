@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-10 text-center">
            @if (\Session::has('success'))
            <div class="alert alert-success ">
                <p>{{ \Session::get('success') }}</p>
            </div>
            @endif

        </div>
        <div class="col-sm-2 text-right ">
            <a class="btn-danger btn my-2" href="{{route('fop.create')}}"> New Record</a>
        </div>
    </div>
    <div class="row justify-content-center">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tag</th>
                    
                    <th scope="col">Edit</th>       
                </tr>
            </thead>
            <tbody>
                @if(sizeof($fops)>0)
                @foreach($fops as $fop)
                <tr class="">  
                    <th scope="row">{{$fop->id}}</th>
                    <td><a class="text-capitalize" href="{{route('fop.show',$fop->id)}}">{{$fop->tag}}</a></td>
                    <td><a href="{{route('fop.edit',['id'=>$fop->id])}}" class="btn btn-sm btn-primary">Edit</a></td>
                    
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $fops->links() }}
        @else
        <div class="col-md-12 alert alert-danger text-center">No records</div>
        @endif
    </div>
</div>
@endsection