@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="col-sm-8 p-2 text-center">
            @if (\Session::has('success'))
            <div class="alert alert-success ">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
            @endif
        </div>
        <form class="col-sm-8" method="POST" action="{{route('fop.update',$fop->id)}}"
            enctype="multipart/form-data">
            {{csrf_field()}}
            @method('PUT')
            <div class="form-group">
                <label for="name">Tag</label>
                <input type="text" class="form-control"  placeholder="Enter tag"
                    value="{{ $fop->tag }}" required name="tag">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection