<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

trait MyTraits
{

    public function upload($request, $type)
    {

        if ($request->has('logo')) {

            $request->validate([
                'logo' =>  'dimensions:min_width=100,min_height=100',
            ]);
            $path = $request->file('logo')->storeAs('public', Carbon::now()->timestamp . '.' . $request->file('logo')->getClientOriginalExtension());
            return  Storage::url($path);
        } else {
            return 'storage/logo.png';
        }
    }
}
