<?php

namespace App\Http\Controllers;

use App\Http\Requests\PracticeRequest;
use App\Http\Resources\PracticeResources;
use App\Models\FieldsOfPractice;
use App\Models\Practice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Traits\MyTraits;

class PracticeController extends Controller
{
    use MyTraits;

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['practices']]);
    }

    public function practices()
    {
        return PracticeResources::collection(Practice::all());
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $practices = Practice::with('tags')->orderBy('id', 'desc')->paginate(5);
        return view(request()->route()->getName())->with('practices', $practices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $tags = FieldsOfPractice::orderBy('tag', 'asc')->get();
        return view(request()->route()->getName())->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $practice = Practice::create([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'logo' => $this->upload($request, 'store'),
        ]);
        $practice->tags()->sync($request->tags);
        return redirect()->route('practices.index')->with('success', 'Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $practice = Practice::with('employees')->whereId($id)->first();
        return view(request()->route()->getName())->with('practice', $practice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function edit(Practice $practice)
    {
        $tags = FieldsOfPractice::orderBy('tag', 'asc')->get();
        return view(request()->route()->getName())->with('practice', $practice)->with('tags', $tags);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function update(PracticeRequest $request, Practice $practice)
    {

        $practice->update([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'logo' => $this->upload($request, 'update'),
        ]);
        $practice->tags()->sync($request->tags);
        return redirect()->route('practices.index')->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Practice $practice)
    {
        $practice->delete();
        return redirect()->route('practices.index')->with('success', 'Updated successfully');
    }
}
