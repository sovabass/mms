<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Practice;

class FieldsOfPractice extends Model
{
    protected $guarded = [];

    public function practices()
    {
        return $this->belongsToMany(Practice::class, 'fop_practice', 'fop_id', 'practice_id');
    }
}
