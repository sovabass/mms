<?php

use App\Models\Employee;
use App\Models\Practice;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        factory(Practice::class, 10)->create()->each(function ($practice) {

            $practice->employees()->saveMany(factory(Employee::class, 10)->make());
        });
    }
}
